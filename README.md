Sender Blacklist
================

An updating list of spammer email addresses.

Exim4 Configuration
-------------------
In exim configuration file find `acl_check_mail` section and add:
```
deny
  senders=/path/to/sender-blacklist/deny_senders
```
