#!/usr/bin/php
<?php

chdir(__DIR__);

$content = file_get_contents('deny_senders');
$lines = explode("\n", $content);

$hosts = [];

foreach ($lines as $line) {
    if ($line === '') continue;
    list($username, $host) = explode('@', $line);
    if (!array_key_exists($host, $hosts)) $hosts[$host] = [];
    if (in_array($username, $hosts[$host])) {
        echo "INFO: dropping duplicate address $username@$host\n";
        continue;
    }
    $hosts[$host][] = $username;
}

ksort($hosts);
$content = '';
foreach ($hosts as $host => $usernames) {
    sort($usernames);
    foreach ($usernames as $username) {
        $content .= "$username@$host\n";
    }
    $content .= "\n";
}

file_put_contents('deny_senders', $content);
